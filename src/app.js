import React, { Component } from 'react'
import { Provider } from 'mobx-react'

import homeStore from './store/home'

import './app.scss'
import './styles/font/iconfont.scss'

const store = {
  homeStore
}

class App extends Component {

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}

  // this.props.children 是将要会渲染的页面
  render () {
    return (
      <Provider store={store}>
        { this.props.children }
      </Provider>
    )
  }
}

export default App
