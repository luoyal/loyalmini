// 这里面集中所有的api请求 并引入当前的api的测试正式环境
import homeService from "./model/home";

export const BASEURL = `https://moyufed.com:8001`

const serviceApi = {
  ...homeService(BASEURL)
}

export default serviceApi
