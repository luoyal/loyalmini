import httpService from "../httpService"; // 引入http请求

const homeService = (BASEURL) => {
  return {
    // 登录接口
    getLoginInfo() {
      return httpService.get({
        url: `${BASEURL}/films`
      })
    }
  }
}
export default homeService
