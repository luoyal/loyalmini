import React, { Component } from 'react'
import { View } from '@tarojs/components'
import { AtList, AtListItem } from 'taro-ui'
import { observer, inject } from 'mobx-react'
import "taro-ui/dist/style/components/icon.scss" // 按需引入
import "taro-ui/dist/style/components/list.scss";

@inject('store')
@observer
class Home extends Component {
  componentWillMount () { }

  // mount
  componentDidMount () {
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  render () {
    const { homeStore: { films } } = this.props.store
    return (
      <View className='index'>
        <AtList>
          {
            films.map((item, idx) => {
              return (
                <AtListItem title={item.title} note={item.director} arrow='right' key={idx} />
              )
            })
          }
        </AtList>
      </View>
    )
  }
}

export default Home
