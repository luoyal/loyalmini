import React, { Component } from 'react'
import { View, Text } from '@tarojs/components'
import { AtButton, AtIcon, AtList, AtListItem } from 'taro-ui'
import Taro from '@tarojs/taro'
import { observer, inject } from 'mobx-react'
import "taro-ui/dist/style/components/button.scss" // 按需引入
import "taro-ui/dist/style/components/icon.scss" // 按需引入
import "taro-ui/dist/style/components/list.scss";

import './index.scss'

@inject('store')
@observer
class Index extends Component {
  componentWillMount () { }

  // mount
  componentDidMount () {
  }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  getFilms = async () => {
    console.log('执行获取数据')
    const { homeStore } = this.props.store
    homeStore.getFilms()
    console.log('执行获取数据完毕')
  }

  increment = () => {
    const { homeStore } = this.props.store
    homeStore.increment()
  }

  toHome = () => {
    Taro.navigateTo({
      url: '/pages/home/home'
    })
  }

  render () {
    const { homeStore: { counter, films } } = this.props.store
    return (
      <View className='index'>
        <Text>Hello world!</Text>
        <AtButton type='primary'>I need Taro UI</AtButton>
        <Text>Taro UI 支持 Vue 了吗？</Text>
        <AtButton type='primary' circle>支持</AtButton>
        <Text>共建？</Text>
        <View className='at-icon at-icon-settings' />
        <AtIcon prefixClass='at' value='star' size='30' color='#F00' />
        <AtIcon prefixClass='icon' value='edit' color='#F00' />

        <AtButton type='secondary' circle onClick={this.getFilms}>获取值</AtButton>

        <AtList>
          {
            films.map((item, idx) => {
              return (
                <AtListItem title={item.title} note={item.director} arrow='right' key={idx} />
              )
            })
          }
        </AtList>

        <AtButton type='secondary' circle onClick={this.increment}> + </AtButton>
        <Text>{counter}</Text>

        <AtButton type='secondary' onClick={this.toHome}> 跳转到home页面 </AtButton>

      </View>
    )
  }
}

export default Index
